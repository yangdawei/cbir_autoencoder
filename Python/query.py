from __future__ import print_function

import argparse
import sys
import json
import numpy as np
import os
# import colorsys
from sklearn.neighbors import NearestNeighbors
from scipy import ndimage
from metrics import histogram_intersection_distance, bhattacharyya_distance, vector_cosine_angle_distance
from bruteknn import BruteKNN

QUERY_IMAGES = '../QueryImages.txt'
DATASET_BASE = '../DataSet/'
ALL_IMAGES = '../AllImages.txt'
QUERY_IMAGES = '../QueryImages.txt'
# COLOR_MODE = 'RGB'

ALL_METRICS = ['L1', 'L2', 'LI', 'HI', 'Bh', 'VCAD']
METRICS_DICT = {
    'HI': (histogram_intersection_distance, True),
    'Bh': (bhattacharyya_distance, True),
    'VCAD': (vector_cosine_angle_distance, True)
}


def main(argv):
    # Parse arguments.
    parser = argparse.ArgumentParser(description='Content-based image retrival.')
    parser.add_argument('--type', '-t', choices=[
                        'build', 'query', 'auto', 'analyze', 'clean'],
                        default='auto', help='Build histograms/Query images/Analyze results/Auto do all the work')

    parser.add_argument(
        '--feature', '-f', choices=['color_hist', 'pca', 'autoencoder'],
        default='color_hist', help='Sselect the feature.')

    parser.add_argument('--bin', '-b', type=int, choices=[16, 128], default=16,
                        help='The number of bins when querying.')
    parser.add_argument('--metric', '-m', choices=ALL_METRICS,
                        default='L2', help='The metrics to measure two vectors.')

    parser.add_argument('--image', '-i', help='Set the images to query.', required=False)

    # parser.add_argument('--mode', choices=[
        # 'RGB', 'YUV', 'HSV'], default='HSV', help='RGB/YUV/HSV mode', required=False)

    args = parser.parse_args(argv)
    print(args)

    # global COLOR_MODE
    # COLOR_MODE = args.mode

    # Clean
    if args.type == 'clean':
        clean_up()

    # Build histograms
    if args.type in ['build', 'auto']:
        build_histograms()

    if args.type == 'build':
        return

    if args.type == 'query':
        query_image(args)

    elif args.type == 'auto':
        query_all()

    if args.type in ['auto', 'analyze']:
        analyze()


def analyze():
    print('Accuracy:')
    for metric in ALL_METRICS:
        print(metric + ':')

        header = metric + '_16bin'
        with open(header + '/res_overall.txt', 'r') as f:
            lines = f.readlines()
            accuracies = map(lambda s: float(s.split()[1]), lines)
            print('\t16bin:', str(sum(accuracies) / len(accuracies)))

        header = metric + '_128bin'
        with open(header + '/res_overall.txt', 'r') as f:
            lines = f.readlines()
            accuracies = map(lambda s: float(s.split()[1]), lines)
            print('\t128bin:', str(sum(accuracies) / len(accuracies)))

    for header in ['PCA16', 'PCA128', 'ATE16']:
        print(header + ':')
        with open(header + '/res_overall.txt', 'r') as f:
            lines = f.readlines()
            accuracies = map(lambda s: float(s.split()[1]), lines)
            print('\t', str(sum(accuracies) / len(accuracies)))

    print('Analyze OK.')


def query_all():
    make_dirs()
    hists16 = json.load(open('bin16.json', 'r'))
    hists128 = json.load(open('bin128.json', 'r'))
    all_trees = build_all_tree(hists16, hists128)

    # Color_histogram based
    for bn in [16, 128]:
        query_hists = gen_query_histograms(default_query(), bn)
        if bn == 16:
            hists = hists16
        else:
            hists = hists128
        for name in ALL_METRICS:
            header = name + '_' + str(bn) + 'bin'
            knn = all_trees[header]
            dists, ind = knn.kneighbors(np.array(query_hists.values()), n_neighbors=30)

            image_precison = dict()
            # for each query image
            for i, query_name in enumerate(query_hists):
                with open(header + '/res_' + query_name[:-4].replace('/', '_') + '.txt', 'w') as f:
                    sort_image_paths = [hists.keys()[k] for k in ind[i]]
                    for j, path in enumerate(sort_image_paths):
                        print(path + ' ' + str(dists[i][j]), file=f)
                    accuracy = compute_accuracy(query_hists.keys()[i], sort_image_paths)
                    image_precison[query_name] = accuracy
                    print(accuracy, file=f)
            with open(header + '/res_overall.txt', 'w') as f:
                for image, precision in image_precison.items():
                    print(image, precision, file=f)

    # PCA and AutoEncoder
    all_and_query = [(json.load(open('pca16.json')), json.load(open('query_pca16.json')), 'PCA16'),
                     (json.load(open('pca128.json')), json.load(open('query_pca128.json')), 'PCA128'),
                     (json.load(open('ate16.json')), json.load(open('query_ate16.json')), 'ATE16')]
    for (hists, query_hists, header) in all_and_query:
        for i, query_name in enumerate(query_hists):
            # Calculate
            knn = pre_query(hists.values(), 'L2')
            dists, ind = knn.kneighbors(np.array(query_hists.values()), n_neighbors=30)

            with open(header + '/res_' + query_name[:-4].replace('/', '_') + '.txt', 'w') as f:
                sort_image_paths = [hists.keys()[k] for k in ind[i]]
                for j, path in enumerate(sort_image_paths):
                    print(path + ' ' + str(dists[i][j]), file=f)
                accuracy = compute_accuracy(query_hists.keys()[i], sort_image_paths)
                image_precison[query_name] = accuracy
                print(accuracy, file=f)
        with open(header + '/res_overall.txt', 'w') as f:
            for image, precision in image_precison.items():
                print(image, precision, file=f)


def query_image(args):
    if args.feature == 'color_hist':
        with open('bin' + str(args.bin) + '.json', 'r') as f:
            hists = json.load(f)
        if args.image is not None:
            query_hists = gen_query_histograms([args.image], args.bin)
        else:
            assert False

    elif args.feature == 'pca':
        with open('pca' + str(args.bin) + '.json', 'r') as f:
            hists = json.load(f)
            query_hists = json.load(open('query_pca' + str(args.bin) + '.json', 'r'))
            args.metric = 'L2'

    elif args.feature == 'autoencoder':
        hists = json.load(open('ate16.json'))
        query_hists = json.load(open('query_ate16.json'))
        args.metric = 'L2'

    knn = pre_query(hists.values(), 'L2')
    dists, ind = knn.kneighbors(query_hists.values(), n_neighbors=len(hists))
    sort_image_paths = [hists.keys()[i] for i in ind[0]]

    for i, path in enumerate(sort_image_paths):
        print(path, dists[0][i])
    print('Accuracy: {}'.format(compute_accuracy(query_hists.keys()[0], sort_image_paths[:30])))


def build_all_tree(hist16, hist128):
    all_trees = dict()
    for name in ALL_METRICS:
        all_trees[name + '_16bin'] = pre_query(hist16.values(), name)
        all_trees[name + '_128bin'] = pre_query(hist128.values(), name)

    return all_trees


def make_dirs():
    for name in ALL_METRICS:
        if not os.path.exists(name + '_16bin'):
            os.makedirs(name + '_16bin')
        if not os.path.exists(name + '_128bin'):
            os.makedirs(name + '_128bin')
    if not os.path.exists('PCA16'):
        os.makedirs('PCA16')
    if not os.path.exists('PCA128'):
        os.makedirs('PCA128')
    if not os.path.exists('ATE16'):
        os.makedirs('ATE16')


def clean_up():
    for name in ALL_METRICS:
        os.system('rm -rf ' + name + '_16bin')
        os.system('rm -rf ' + name + '_128bin')

    os.system('rm -rf bin16.json')
    os.system('rm -rf bin128.json')

    print('Clean up OK.')


def compute_accuracy(query_name, name_list):
    mcat = query_name.split('/')[0]
    count = 0
    for name in name_list:
        cat, ignored = name.split('/')
        if cat == mcat:
            count += 1

    return float(count) / len(name_list)


def gen_query_histograms(image_files, bin_num):
    hists = dict()
    for path in image_files:
        # mat = convert(ndimage.imread(DATASET_BASE + path))
        mat = ndimage.imread(DATASET_BASE + path)
        hists[path] = histogram_of_image(mat, bin_num)

    return hists


def build_histograms():
    paths = list()
    with open(ALL_IMAGES, 'r') as f:
        for line in f:
            if len(line) == 0:
                continue
            try:
                path, width, height = line.split()
                paths.append(path)
            except ValueError:
                continue

    path_hist16 = dict()
    path_hist128 = dict()
    for path in paths:
        # image = convert(ndimage.imread(DATASET_BASE + path))
        image = ndimage.imread(DATASET_BASE + path)
        path_hist16[path] = histogram_of_image(image, bin_num=16)
        path_hist128[path] = histogram_of_image(image, bin_num=128)

    with open('bin16.json', 'w') as f:
        json.dump(path_hist16, f)
    with open('bin128.json', 'w') as f:
        json.dump(path_hist128, f)

    print('Build histograms OK.')


def pre_query(histograms, metric):
    # Bh obey triangular inequaties.
    if metric == 'L1':
        return NearestNeighbors(n_neighbors=30, metric='manhattan').fit(histograms)
    elif metric == 'L2':
        return NearestNeighbors(n_neighbors=30).fit(histograms)
    elif metric == 'LI':
        return NearestNeighbors(n_neighbors=30, metric='chebyshev').fit(histograms)
    else:
        method, is_metric = METRICS_DICT[metric]
        if is_metric:
            return NearestNeighbors(n_neighbors=30, algorithm='ball_tree',
                                    metric='pyfunc', func=method).fit(histograms)
        else:
            return BruteKNN(n_neighbors=30, metric=method).fit(histograms)


def histogram_of_image(image, bin_num):
    h, w, d = image.shape

    if bin_num == 16:
        bins = (2, 4, 2)
    elif bin_num == 128:
        bins = (4, 8, 4)
    else:
        raise ValueError('Invalid bin_num %d' % bin_num)

    hist, rng = np.histogramdd(image.swapaxes(0, 2).reshape(3, h * w).T, bins=bins, range=((0, 255),) * 3)
    hist /= h * w
    return hist.ravel().tolist()


def default_query():
    queries = []
    with open(QUERY_IMAGES, 'r') as f:
        for line in f:
            if len(line) == 0:
                continue
            try:
                query, width, height = line.split()
                queries.append(query)
            except ValueError:
                continue

    return queries


# def convert(mat):
#     if COLOR_MODE == 'RGB':
#         return mat
#     elif COLOR_MODE == 'HSV':
#         ret = np.ndarray(mat.shape, np.float)
#         h, w, d = mat.shape
#         for i in range(h):
#             for j in range(w):
#                 ret[i, j, 0], ret[i, j, 1], ret[i, j, 2] = colorsys.rgb_to_hsv(
#                     mat[i, j, 0]/255.0, mat[i, j, 1]/255.0, mat[i, j, 2]/255.0)
#         return ret

if __name__ == '__main__':
    main(sys.argv[1:])
