import numpy as np
import math


def histogram_intersection_distance(vecX, vecY):
    return 1 - np.sum(np.minimum(vecX, vecY))


def bhattacharyya_distance(vecX, vecY):
    return 1 - np.sum(np.sqrt(vecX * vecY))


def vector_cosine_angle_distance(vecX, vecY):
    dot_r = np.dot(vecX, vecY)
    len_r = math.sqrt(vecX.dot(vecX) * vecY.dot(vecY))
    ret = math.acos(min(dot_r / len_r, 1))
    return ret
    return 1 - dot_r
