import numpy as np


class BruteKNN(object):

    '''
    Brute-force K nearest neighbors search.
    The time-complexity is O(nlogn).
    '''

    def __init__(self, n_neighbors, metric):
        self.__n_neighbors = n_neighbors
        self.__metric = metric

    def fit(self, X):
        self.__data = np.array(X, np.float)
        return self

    def kneighbors(self, values, n_neighbors=None):
        '''
        Brute-force search.
        '''
        dist_mat = np.ndarray([len(values), len(self.__data)], dtype=np.float)
        for i, value in enumerate(values):
            for j, data in enumerate(self.__data):
                dist_mat[i, j] = self.__metric(value, data)
        indices = np.argsort(dist_mat, axis=1)

        dist_mat.sort(axis=1)

        return dist_mat[:, :n_neighbors], indices[:, :n_neighbors]
