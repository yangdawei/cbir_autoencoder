#include <cassert>
#include <vector>
#include <cstdio>
#include <armadillo>
#include <opencv2/opencv.hpp>
#include <io.h>
#include <direct.h>

#include "misc.h"
#include "AutoEncoder.h"

using namespace std;
using namespace arma;
using namespace cv;

arma::mat pca_compress(mat data_all, mat data_query, mat *reconstruction, int max_dim)
{
	mat u = mean(data_all, 1);
	mat data_norm = data_all - repmat(u, 1, data_all.n_cols);
	mat covar = data_norm * data_norm.t();
	mat U, V;
	vec S;

	svd(U, S, V, covar);

	mat W = V.cols(0, max_dim);
	if (reconstruction != NULL)
		*reconstruction = W * W.t() * data_norm + repmat(u, 1, data_all.n_cols);

	return W.t() * (data_query - repmat(u, 1, data_query.n_cols));
}

arma::mat autoencoder_compress(mat data_all, mat data_query, mat *reconstruction, bool cached)
{
	const int rbm_layers[] = {512, 256, 128, 16}; // the hidden layers, not including the first input layer
	const RBM::UnitType rbm_layers_hidden_type[] = { RBM::SIGMOID, RBM::SIGMOID, RBM::SIGMOID, RBM::LINEAR };
	// const int rbm_layers[] = {512, 8}; // the hidden layers, not including the first input layer
	// const RBM::UnitType rbm_layers_hidden_type[] = {RBM::SIGMOID, RBM::LINEAR};
	const RBM::UnitType first_layer_visible_type = RBM::LINEAR; // Use LINEAR for real value data, else SIGMOID for binary
	assert(sizeof(rbm_layers) == sizeof(rbm_layers_hidden_type));

	// RBM - trainig parameters
	const int num_rbms = sizeof(rbm_layers) / sizeof(int);
	//const int mini_batches = 100;
	const int mini_batches = 81 * 9;
	const int iterations = 1000;
	const double momentum_rate = 0.9;
	const double weight_decay = 0.0; // not tested
	const bool early_stopping = true;

	vector <RBM> rbms(num_rbms);
	AutoEncoder autoencoder;

	cout << "Total training data: " << data_all.n_cols << endl;

	double u, s;
	mat data_norm = RBM::Normalise(data_all, &u, &s);

	if (!cached)
	{
		mat data_train = data_norm;

		// Stack train the RBM
		{
			rbms[0].Train(data_train, first_layer_visible_type, rbm_layers_hidden_type[0], rbm_layers[0], mini_batches, iterations, 0.001, momentum_rate, weight_decay, early_stopping);
			data_train = rbms[0].FeedForward(data_train);

			// middle layer is all sigmoid
			for (int i = 1; i < num_rbms; i++) 
			{
				double learning_rate;

				if (rbm_layers_hidden_type[i - 1] == RBM::LINEAR || rbm_layers_hidden_type[i] == RBM::LINEAR) {
					learning_rate = 0.005;
				}
				else {
					learning_rate = 0.1;
				}

				rbms[i].Train(data_train, rbm_layers_hidden_type[i - 1], rbm_layers_hidden_type[i], rbm_layers[i], mini_batches, iterations, learning_rate, momentum_rate, weight_decay, early_stopping);

				data_train = rbms[i].FeedForward(data_train);
			}
		}


		autoencoder.SetRBMHalf(rbms); // this will mirror the RBMS
		autoencoder.BackPropagation(data_norm, 100, 0.01, momentum_rate, early_stopping);

		const char *serialization = "autoencoder_data";
		if (_access(serialization, 0) == -1)
			_mkdir(serialization);

		autoencoder.Serialize(serialization);
	}
	else
	{
		autoencoder.SetRBMHalf(rbms);
		// printf("%d\n", autoencoder.m_rbms.size());
		// system("pause");
		const char *serialization = "autoencoder_data";
		autoencoder.Unserialize(serialization);
		// printf("%d\n", autoencoder.m_rbms.size());
		// system("pause");
		/*
		for (size_t i = 0; i < autoencoder.m_rbms.size(); i++)
			printf("%d %d\n", autoencoder.m_rbms[i].m_weights.n_rows, autoencoder.m_rbms[i].m_weights.n_cols);
		 */
	}
		// Reconstruction error
		/*
		reconstruction[c] = autoencoder.FeedForward(data_norm);
		reconstruction[c] = reconstruction[c] * s + u;

		mat err = data[c] - reconstruction[c];
		err = err % err; // err .* err
		double rmse = sqrt(accu(err) / err.n_elem);
		cout << "Reconstruction error: " << rmse << endl;
		//DisplayReconstruction(data, reconstruction);
		PCA_reconstruction[c] = RunPCA(data[c]);

		data[c] = data[c].t();
		reconstruction[c] = reconstruction[c].t();
		PCA_reconstruction[c] = PCA_reconstruction[c].t();
		*/
	//SaveImages(data, reconstruction, PCA_reconstruction);

	if (reconstruction != NULL)
	{
		*reconstruction = autoencoder.FeedForward(data_norm);
		*reconstruction = *reconstruction * s + u;
	}


	return autoencoder.Encode(data_query);
}

void compress()
{
	vector<string> image_files = AllNameList();

	const string base_dir = "../DataSet/";
	const string other_base = "../compressed/";

	for (const string &_str : image_files)
	{
		cv::Mat image = imread(base_dir + _str, CV_LOAD_IMAGE_COLOR);
		resize(image, image, Size(IMAGE_SIZE, IMAGE_SIZE));

		cv::imwrite(other_base + _str, image);
	}
}

vector<string> NameList(const char *filename)
{
	ifstream fin(filename);
	string str;
	char total[4096];
	int width, height;
	vector<string> image_files;

	while (getline(fin, str))
	{
		sscanf(str.c_str(), "%s %d %d", total, &width, &height);
		printf("%s\n", total);
		image_files.push_back(total);
	}

	return image_files;
}

vector<string> QueryNameList()
{
	return NameList("../QueryImages.txt");
}
vector<string> AllNameList()
{
	return NameList("../AllImages.txt");
}

mat LoadData(const vector<string> &all_name);

mat LoadImageData()
{
	return LoadData(AllNameList());
}

mat LoadQueryData()
{
	return LoadData(QueryNameList());
}

mat LoadData(const vector<string> &all_name)
{
	const char *other_dir = "../compressed/";
	if (_access(other_dir, 0) == -1)
		_mkdir(other_dir);

	mat all_data(IMAGE_SIZE * IMAGE_SIZE * 3, all_name.size());

	for (size_t i = 0; i < all_name.size(); i++)
	{
		cv::Mat image = cv::imread(other_dir + all_name[i]);
		// cout << other_dir + all_name[i] << endl;
		// cout << image.cols << image.rows << endl;
		// cout << image.size() << endl;
		for (size_t j = 0; j < IMAGE_SIZE * IMAGE_SIZE; j++)
		{
			// cout << j << endl;
			all_data.at(3 * j + 0, i) = image.at<Vec3b>(j)[0];
			all_data.at(3 * j + 1, i) = image.at<Vec3b>(j)[1];
			all_data.at(3 * j + 2, i) = image.at<Vec3b>(j)[2];
		}
	}

	return all_data;
}

void SaveImageData(const mat &data, const char *dir)
{
	char other_dir[4096];
	sprintf(other_dir, "../%s/", dir);
	if (_access(other_dir, 0) == -1)
		_mkdir(other_dir);

	vector<string> all_name = AllNameList();
	cv::Mat image(IMAGE_SIZE, IMAGE_SIZE, CV_8UC3);
	for (size_t i = 0; i < all_name.size(); i++)
	{
		for (size_t j = 0; j < IMAGE_SIZE * IMAGE_SIZE; j++)
		{
			image.at<Vec3b>(j)[0] = saturate_cast<uchar>(data.at(3 * j + 0, i));
			image.at<Vec3b>(j)[1] = saturate_cast<uchar>(data.at(3 * j + 1, i));
			image.at<Vec3b>(j)[2] = saturate_cast<uchar>(data.at(3 * j + 2, i));
		}
		waitKey(0);
		cout << other_dir + all_name[i] << endl;
		cv::imwrite(other_dir + all_name[i], image);
	}
}

void jsonize(const char *filename, const std::vector<std::string> &header, const arma::mat &data)
{
	FILE *fp = fopen(filename, "w");
	fputs("{\n", fp);
	{
		for (size_t i = 0; i < header.size(); i++)
		{
			fprintf(fp, "\t\"%s\" : ", header[i].c_str());
			fputs("[", fp);
			{
				for (size_t j = 0; j < data.n_rows; j++)
				{
					fprintf(fp, "%.10f", data.at(j, i));
					if (j != data.n_rows - 1)
						fputs(",", fp);
				}
			}
			fputs("]", fp);
			if (i != header.size() - 1)
				fputs(",", fp);
			fputs("\n", fp);
		}
	}
	fputs("}", fp);
	fclose(fp);
}
