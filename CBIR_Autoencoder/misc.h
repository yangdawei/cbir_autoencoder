#pragma once

#include <armadillo>
#include <vector>
#include <string>

#define IMAGE_SIZE 32

// Use autoencoder to compress the image data. The column of 'data_all' corresponds to an gray-scale image.
// if reconstruction != NULL, then a reconstruction data will be stored.
// If cached, then autoencoder is loaded using the data stored on the disk.
// Otherwise, an autoencoder is trained and store on the disk.
arma::mat autoencoder_compress(arma::mat data_all, arma::mat data_query, arma::mat *reconstruction, bool cached);

/**
 * Compress using pca. See autoencoder_compress.
 */
arma::mat pca_compress(arma::mat data_all, arma::mat data_query, arma::mat *reconstruction, int max_dim);

/**
 * Load all the image data using information in AllImages.txt.
 */
arma::mat LoadImageData();

arma::mat LoadQueryData();

/**
 * Save images data in 'data' to directory 'dir'.
 */
void SaveImageData(const arma::mat &data, const char *dir);

/**
 * Get all the image file names from AllImages.txt.
 */
std::vector<std::string> AllNameList();

/**
 * Get all the query image file names from QueryImages.txt
 */
std::vector<std::string> QueryNameList();

/**
 * Compress all the images to 32x32 and store to directory '32x32'.
 */
void compress();

/**
 * Transform the histograms to a python dict.
 */
void jsonize(const char *filename, const std::vector<std::string> &header, const arma::mat &data);
