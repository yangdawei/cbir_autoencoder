#include "misc.h"

using namespace std;
using namespace arma;

int main(int argc, char *argv)
{
	compress();

	vector<string> header = AllNameList();

	mat m = LoadImageData();
	mat q = LoadQueryData();
	
	mat recon;

	/*
	mat cmps_pca = pca_compress(m, m, &recon, 16);
	SaveImageData(recon, "pca");
	jsonize("pca16.json", header, cmps_pca);

	cmps_pca = pca_compress(m, m, &recon, 128);
	SaveImageData(recon, "pca");
	jsonize("pca128.json", header, cmps_pca);
	*/

	mat cmps_ate = autoencoder_compress(m, m, &recon, false);
	cout << cmps_ate.n_rows << cmps_ate.n_cols << endl;
	SaveImageData(recon, "ate");
	jsonize("ate16.json", header, cmps_ate);

	/*
	header = QueryNameList();
	cmps_pca = pca_compress(m, q, NULL, 16);
	jsonize("query_pca16.json", header, cmps_pca);

	cmps_pca = pca_compress(m, q, NULL, 128);
	jsonize("query_pca128.json", header, cmps_pca);
	*/

	cmps_ate = autoencoder_compress(m, q, NULL, true);
	jsonize("query_ate16.json", header, cmps_ate);
}
